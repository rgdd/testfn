package testfn

import "fmt"

func Format(s string, i int) string {
	return fmt.Sprintf("%s: %d", s, i)
}

func Add(a, b int) int {
	return a + b
}

func Sub(a, b int) int {
	return a - b
}

func Mul(a, b int) int {
	return a * b
}
